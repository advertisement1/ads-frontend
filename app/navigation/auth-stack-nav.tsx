import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {Login, Register, OnBoarding} from '../screen';

const Stack = createStackNavigator();

export function AuthStackNav() {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="OnBoarding">
      <Stack.Screen name="OnBoarding" component={OnBoarding} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Login" component={Login} />
    </Stack.Navigator>
  );
}
