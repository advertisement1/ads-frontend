import * as React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Icon} from 'react-native-elements';

import {Home, Categories} from '../screen';

const Tab = createBottomTabNavigator();

interface Icon {
  type: string;
  name: string;
}

export function BottomNavigation() {
  // const {GREEN, BLACK} = COLORS;

  function menuSingle(route: any, tabBarIcon: any, icon: Icon) {
    const {color, size, focused} = tabBarIcon;

    function renderIcon() {
      switch (route.name) {
        case 'Home':
          return (
            <Icon
              name={icon.name}
              type={icon.type}
              onPress={() => console.log('hello')}
            />
          );
        case 'Categories':
          return (
            <Icon
              name={icon.name}
              type={icon.type}
              onPress={() => console.log('hello')}
            />
          );
        default:
          return (
            <Icon
              name={icon.name}
              type={icon.type}
              onPress={() => console.log('hello')}
            />
          );
      }
    }

    return (
      <View style={styles.menu}>
        {renderIcon()}
        <Text style={[styles.menuName, {color: focused ? 'green' : 'black'}]}>
          {route.name}
        </Text>
      </View>
    );
  }

  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        activeTintColor: 'green',
        inactiveTintColor: 'black',
        style: {
          elevation: 20,
          height: 60,
          borderTopLeftRadius: 15,
          borderTopRightRadius: 15,
          backgroundColor: '#fff',
          position: 'absolute',
        },
      }}
      screenOptions={({route}) => ({
        tabBarIcon: (tabBarIcon: any) => {
          if (route.name === 'Home') {
            const icon = {
              type: 'font-awesome',
              name: 'home',
            };
            return menuSingle(route, tabBarIcon, icon);
          } else if (route.name === 'Categories') {
            const icon = {
              type: 'material-community',
              name: 'clipboard-list-outline',
            };
            return menuSingle(route, tabBarIcon, icon);
          }
        },
      })}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Categories" component={Categories} />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  menu: {
    alignItems: 'center',
  },
  menuName: {
    fontSize: 12,
    fontWeight: '700',
  },
});
