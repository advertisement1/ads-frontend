interface Colors {
  primary: string;
  primary_light: string;
  primary_dark: string;
  secondary: string;
  secondary_light: string;
  secondary_dark: string;
  text_on_p: string;
  text_on_s: string;
}

export const colors: Colors = {
  primary: '#00c853',
  primary_light: '#5efc82',
  primary_dark: '#009624',
  secondary: '#00bfa5',
  secondary_light: '#5df2d6',
  secondary_dark: '#008e76',
  text_on_p: '#000000',
  text_on_s: '#000000',
};
