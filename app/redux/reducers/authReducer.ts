interface ActionI {
  type: string;
  payload: any;
}

const initial_state = {
  isLogged: false,
};

const authReducer = (state = initial_state, {type, payload}: ActionI) => {
  switch (type) {
    default:
      return state;
  }
};

export default authReducer;
