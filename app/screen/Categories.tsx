import React from 'react';
import {StatusBar, StyleSheet, Text, View} from 'react-native';

const Categories = () => {
  return (
    <View style={styles.container}>
      <Text>Categories screen</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Categories;
