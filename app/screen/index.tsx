import Home from './Home';
import Categories from './Categories';

import Register from './auth/Register';
import Login from './auth/Login';
import OnBoarding from './auth/OnBoarding';

export {Home, Categories, Register, Login, OnBoarding};
