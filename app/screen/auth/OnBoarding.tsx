import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';

import {colors} from '../../constants';

const OnBoarding = props => {
  const navigate = (screen: string) => props.navigation.navigate(screen);

  return (
    <View style={styles.container}>
      <Button
        title="Login to existing account"
        type="outline"
        raised
        containerStyle={styles.buttonContainer}
        titleStyle={{color: colors.primary}}
        buttonStyle={{borderColor: colors.primary}}
        onPress={() => navigate('Login')}
      />
      <Button
        title="Create a new account"
        type="solid"
        raised
        containerStyle={styles.buttonContainer}
        buttonStyle={{backgroundColor: colors.primary}}
        onPress={() => navigate('Register')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonContainer: {
    marginBottom: 20,
    width: '90%',
  },
});

export default OnBoarding;
