import React from 'react';
import {StatusBar, StyleSheet, Text, View} from 'react-native';
import {useSelector} from 'react-redux';

import {BottomNavigation, AuthStackNav} from './app/navigation';
import {colors} from './app/constants';

const App = () => {
  const auth = useSelector(state => state.auth);

  return (
    <>
      <StatusBar backgroundColor={colors.primary} />
      {auth.isLogged ? <BottomNavigation /> : <AuthStackNav />}
    </>
  );
};

const styles = StyleSheet.create({});

export default App;
